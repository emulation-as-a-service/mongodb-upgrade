#!/bin/sh

docker-compose-bin=docker-compose

if ! command -v $docker-compose-bin &> /dev/null
then
    docker-compose-bin=/usr/local/bin/docker-compose
    if ! command -v $docker-compose-bin  &> /dev/null
    then
        echo "COMMAND could not be found"
        exit
    fi 
fi

$docker-compose-bin build

docker run -v $1:/data/db -t eaas/mongo-upgrade-30 
docker run -v $1:/data/db -t eaas/mongo-upgrade-32
docker run -v $1:/data/db -t eaas/mongo-upgrade-34 
docker run -v $1:/data/db -t eaas/mongo-upgrade-36 
