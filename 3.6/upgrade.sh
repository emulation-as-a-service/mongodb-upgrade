#!/bin/sh

## prepare services
/usr/bin/mongod --dbpath /data/db --smallfiles --fork --logpath /tmp/mongo
mongo --eval 'db.adminCommand( { setFeatureCompatibilityVersion: "3.6" } )'
/usr/bin/mongod --shutdown --dbpath /data/db


